# Microservice Reference Architecture

A super simple ecommerce application as a microservice architecture.

# Getting Started

Tools used for local development:

* minikube
* Docker Desktop
* kubectl
* k9s
* Intellij IDEA
* nvm
* Java 15

# Deployment instructions

Make sure minikube is running. Run the following to deploy the entire application to local minikube:

```shell script
mvn clean install
```

Run `minikube tunnel` and go to http://localhost:3000 , you should see the default React page. Clicking on "Search" will trigger the call to the search service backend.

## Manual process

This will all be automated in mvn soon, leaving as is.

1. Create images and container. Quit container after starting.

```shell script
docker build -t ovh-admin ./backend/ovh-admin
docker run --name ovh-admin -it ovh-admin:latest

docker build -t ovh-player ./backend/ovh-player
docker run --name ovh-player -it ovh-player:latest
```

2. Push images to repository

```shell script
docker commit -m "Message here" -a "your name here" ovh-admin $USERNAME/ovh-admin:latest
docker push $USERNAME/ovh-admin

docker commit -m "Message here" -a "your name here" ovh-player $USERNAME/ovh-player:latest
docker push $USERNAME/ovh-player
```

3. Deploy kubernetes

```shell script
minikube start
kubectl delete -f deployment.yml
kubectl apply -f deployment.yml
```

4. Tunnel minikube to host

```shell script
minikube tunnel
```