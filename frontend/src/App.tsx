import React from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';


const App: React.FC = () => {
    let [results, setResults] = React.useState<string>('hello')
    
    const callSearch = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        
        axios({
            "method": "GET",
            "url": "/search-service/ping"
        }).then(({data}) => {
            setResults(data);
        }).catch((error) => {
            console.log(error);
        });
    }

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.tsx</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
                <a onClick={callSearch}>Search service says: {results}</a>
            </header>
        </div>
    );
}

export default App;
