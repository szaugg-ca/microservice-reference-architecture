package com.ca.search.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin(origins = "*")
public class AdminController {
    
    @Autowired
    RestTemplate restTemplate;

    @Value("${app.cart.url}")
    public String cartUrl;
    
    @GetMapping("/ping")
    public String ping() {
        return "pong";
    }
    
    @GetMapping("/ping/player")
    public String pingPlayer() {
        String result = "Player says: ";
        
        String response = restTemplate.getForObject(cartUrl + "/ping", String.class);
        
        return result + response;
    }
}
